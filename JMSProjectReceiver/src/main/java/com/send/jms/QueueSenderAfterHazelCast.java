package com.send.jms;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;

import com.java.HazelCast.HazelCastCheckValues;

public class QueueSenderAfterHazelCast {

	public static String xmlobject = "";

	public static void afterXSLT(String eid, String msg) throws Exception {
		boolean keyFound = HazelCastCheckValues.checkEidInHazelCast(eid);
		if (keyFound) {
			ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("admin", "admin",
					"tcp://devopsapp.southcentralus.cloudapp.azure.com:61616");
			Connection connection = connectionFactory.createConnection();
			connection.start();
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			Destination queue = session.createQueue("requestQAfterXSLT");
			MessageProducer producer = session.createProducer(queue);
			TextMessage message = session.createTextMessage(msg);
			producer.send(message);
			connection.close();
		} else {
			System.out.println("Employee Info Not Found in Hazelcast.. Unable to post in MQ...");
		}

	}

}
