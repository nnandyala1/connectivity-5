package com.java.HazelCast;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;


import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.client.config.ClientNetworkConfig;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;

public class HazelCastCheckValues {

	public static boolean checkEidInHazelCast(String eid2) {
		List<String> list = new ArrayList<String>();

		// for server
		ClientConfig clientConfig = new ClientConfig();
		clientConfig.getGroupConfig().setName("dev");
		clientConfig.getGroupConfig().setPassword("dev-pass");
		ClientNetworkConfig networkConfig = clientConfig.getNetworkConfig();
		// networkConfig.addAddress("13.84.52.95","13.84.52.95:5701");
		try {
			networkConfig.addAddress(InetAddress.getLocalHost().getHostAddress() + ":5701");
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		HazelcastInstance hci = HazelcastClient.newHazelcastClient(clientConfig);

		// for local
		// HazelcastInstance hci = Hazelcast.newHazelcastInstance();

		IMap<String, String> emp = hci.getMap("Employees");
		emp.put("Deepu", "125");
		emp.put("Sri", "98");
		emp.put("Pramod", "420");
		emp.put("Surya", "100");
		emp.put("Ajay", "512");
		System.out.println("Cache Value..............."+emp.size());
		//System.out.println(emp.values().toString());
		for (String name : emp.values()) {
			list.add(name);
		}
		if (list.contains(eid2)) {

			System.out.println("Found Employee Information :: " + eid2);
			return true;
		} else {
			return false;
		}

	}
}