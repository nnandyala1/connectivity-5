package com.send.jms;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyBoolean;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.java.HazelCast.HazelCastCheckValues;



@RunWith(PowerMockRunner.class)
@PrepareForTest({ QueueSenderAfterHazelCast.class, ActiveMQConnectionFactory.class, Queue.class, ConnectionFactory.class, Connection.class,
		Session.class, Destination.class, MessageProducer.class, TextMessage.class, HazelCastCheckValues.class })
public class QueueSenderAfterHazelCastTest {

	@Mock
	ActiveMQConnectionFactory connectionFactory;

	@Mock
	Connection connection;

	@Mock
	Session session;

	@Mock
	Queue queue;

	@Mock
	MessageProducer msgproducer;

	@Mock
	TextMessage message;
	
	@Mock
	HazelCastCheckValues hccv;

	@Test
	public void testQueueSenderAfterHazelCastIfBlock() {
		try {
			connectionFactory = PowerMockito.mock(ActiveMQConnectionFactory.class);
			connection = PowerMockito.mock(Connection.class);
			session = PowerMockito.mock(Session.class);
			queue = PowerMockito.mock(Queue.class);
			msgproducer = PowerMockito.mock(MessageProducer.class);
			message = PowerMockito.mock(TextMessage.class);
			hccv = PowerMockito.mock(HazelCastCheckValues.class);
			
			PowerMockito.whenNew(ActiveMQConnectionFactory.class).withArguments("admin","admin" ,"tcp://devopsapp.southcentralus.cloudapp.azure.com:61616" ).thenReturn(connectionFactory);
			PowerMockito.when(connectionFactory.createConnection()).thenReturn(connection);
			
			PowerMockito.when(connection.createSession(Mockito.any(Boolean.class), Mockito.any(Integer.class))).thenReturn(session);
			PowerMockito.when(session.createQueue(Mockito.anyString())).thenReturn(queue);
			PowerMockito.when(session.createProducer(queue)).thenReturn(msgproducer);
			PowerMockito.when(session.createTextMessage(Mockito.anyString())).thenReturn(message);
			PowerMockito.mockStatic(HazelCastCheckValues.class);
			PowerMockito.when(HazelCastCheckValues.checkEidInHazelCast(Mockito.anyString())).thenReturn(true);
			QueueSenderAfterHazelCast.afterXSLT("512", "Dummy Message");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testQueueSenderAfterHazelCastElseBlock() {
		try {
			PowerMockito.mockStatic(HazelCastCheckValues.class);
			PowerMockito.when(HazelCastCheckValues.checkEidInHazelCast(Mockito.anyString())).thenReturn(false);
			QueueSenderAfterHazelCast.afterXSLT("512", "Dummy Message");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
